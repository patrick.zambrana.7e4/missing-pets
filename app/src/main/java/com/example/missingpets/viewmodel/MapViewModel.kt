package com.example.missingpets.viewmodel

import android.app.Application
import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.missingpets.MainActivity
import com.example.missingpets.R
import com.example.missingpets.model.PetMark
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import kotlin.coroutines.coroutineContext

class MapViewModel(application: Application) : AndroidViewModel(application) { // PROVISIONAL: It is needed just for display images from drawable for TEST data

    private val context = getApplication<Application>().applicationContext
    val db = FirebaseFirestore.getInstance()
    var dataIsReady = MutableLiveData(false)

    object currentMark {
        var marks = MutableLiveData<MutableList<PetMark>>(mutableListOf())
        var currentMarker = MutableLiveData<Marker>()
        var editMark = MutableLiveData<PetMark>()
        var markerToZoom: PetMark? = null
    }

    companion object {
        var currentUser: String? = null
    }

    init { // MAKE CALL AUTOMATIC WHEN ENTER TO MARKER LIST
        var list = mutableListOf<PetMark>()
        db.collection("markers")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val marker = PetMark(document.id,
                        document.get("user") as String?,
                        document.get("description") as String,
                        document.get("tlf") as String,
                        document.get("email") as String,
                        document.get("image") as String?,
                        document.get("location") as GeoPoint,
                        document.get("markTitle") as String,
                        document.get("address") as String
                    )
                    list.add(marker)
                    currentMark.marks.value = list
                    Log.d(TAG, "${document.id} => ${document.data}")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "Error getting documents: ", exception)
            }

        //dataIsReady.value = true
        Log.i("TEST", "${currentMark.marks.value}")
    }
}

/*
list.add(
            PetMark(1,
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla velit ante, non interdum orci pharetra et.",
                "690195047",
                "conan.doyle@gmail.com",
                //ContextCompat.getDrawable(context, R.drawable.dog_lost),
                LatLng(41.681413, 1.321569),
                "Small white dog",
                "c/Sant Pau, 231"
            )
        )
        list.add(
            PetMark(2,
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla velit ante, non interdum orci pharetra et.",
                "690195347",
                "irina.luminesk@gmail.com",
                //ContextCompat.getDrawable(context, R.drawable.cat_lost),
                LatLng(43.681413, 13.321569),
                "Brown, black & white cat",
                "c/Char, 34"
            )
        )

 */