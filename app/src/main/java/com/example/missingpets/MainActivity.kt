package com.example.missingpets

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.missingpets.ui.HomeFragment
import com.example.missingpets.viewmodel.MapViewModel
import com.google.android.gms.maps.GoogleMap


class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private lateinit var viewModel : MapViewModel
    private var isLogged: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this)[MapViewModel::class.java]
        loadUserData()

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController

        if (!isLogged)
            navController.navigate(R.id.action_homeFragment_to_loginFragment)
    }

    private fun loadUserData() {
        val sharedPreferences = getSharedPreferences("UserCredentials", Context.MODE_PRIVATE)
        val email = sharedPreferences.getString("USERNAME", null)
        val password = sharedPreferences.getString("PASSWORD", null)
        MapViewModel.currentUser = email

        if (email != null && password != null)
            isLogged = true
    }


}