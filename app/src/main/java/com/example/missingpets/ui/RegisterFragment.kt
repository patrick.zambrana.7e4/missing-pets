package com.example.missingpets.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.example.missingpets.R
import com.example.missingpets.viewmodel.MapViewModel
import com.example.missingpets.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import android.content.Intent.getIntent

import android.content.Intent

import androidx.annotation.NonNull

import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.auth.FirebaseUser




class RegisterFragment : Fragment() {
    private val viewModel: MapViewModel by viewModels()
    private var _binding: FragmentRegisterBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var email: String
        var password: String
        var confirmPass: String

        binding.registerButton.setOnClickListener {
            email = binding.username.text.toString()
            password = binding.password.text.toString()
            confirmPass = binding.confirmPassword.text.toString()

            if (password.equals(confirmPass)) {
                FirebaseAuth.getInstance().
                createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener {
                        if(it.isSuccessful){
                            val emailLogged = it.result?.user?.email
                            //goToHome(emailLogged!!)
                            val user = FirebaseAuth.getInstance().currentUser
                            user!!.sendEmailVerification()
                            NavHostFragment.findNavController(this).navigate(R.id.action_registerFragment_to_loginFragment)
                            Toast.makeText(context, "A confirmation email have been send to your account.", Toast.LENGTH_SHORT).show()
                        }
                        else{
                            Toast.makeText(context, "Hi ha algún error amb les dades", Toast.LENGTH_SHORT).show()
                        }
                    }
            }

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}