package com.example.missingpets.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.missingpets.MainActivity
import com.example.missingpets.R
import com.example.missingpets.viewmodel.MapViewModel
import com.example.missingpets.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth
import java.util.jar.Manifest
import android.preference.PreferenceManager

import android.content.SharedPreferences

class LoginFragment : Fragment() {

    private val viewModel: MapViewModel by viewModels()
    private var _binding: FragmentLoginBinding? = null
    private val SHARED_PREFERENCES_FILE = "UserCredentials"

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        val root: View = binding.root

        var email: String
        var password: String

        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("IsLogin", false))
            NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_homeFragment)

        binding.loginButton.setOnClickListener {
            email = binding.email.text.toString()
            password = binding.password.text.toString()

            if (!email.equals("") && !password.equals("")) {
                FirebaseAuth.getInstance().
                signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener {
                        if(it.isSuccessful){
                            val emailLogged = it.result?.user?.email
                            //goToHome(emailLogged!!)
                            NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_homeFragment)
                            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
                            saveCredentials()
                            prefs.edit().putBoolean("Islogin", true).apply()
                            binding.loginButton.isClickable = false
                        }
                        else{
                            binding.loginButton.isClickable = true
                            Toast.makeText(context, it.exception?.message, Toast.LENGTH_SHORT).show()
                        }
                    }

            }
        }
        binding.registerButton.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_registerFragment)
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        binding.loginButton.isClickable = true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun saveCredentials() {
        val email = binding.email.text.toString()
        val password = binding.password.text.toString()
        val sharedPref = activity?.getSharedPreferences(SHARED_PREFERENCES_FILE, Context.MODE_PRIVATE)
        val editor = sharedPref!!.edit()
        editor.apply {
            putString("USERNAME", email)
            putString("PASSWORD", password)
        }.apply()

        Toast.makeText(context, "Logged in.", Toast.LENGTH_SHORT).show()
    }

}