package com.example.missingpets.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.example.missingpets.R
import com.example.missingpets.databinding.FragmentHomeBinding
import com.example.missingpets.viewmodel.MapViewModel
import com.example.missingpets.viewmodel.MapViewModel.currentMark.markerToZoom
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore

lateinit var map : GoogleMap
const val REQUEST_CODE_LOCATION = 100

class HomeFragment : Fragment(), OnMapReadyCallback {

    private val viewModel: MapViewModel by viewModels()
    private var _binding: FragmentHomeBinding? = null
    private var currentMarker: Marker? = null
    private val db = FirebaseFirestore.getInstance()

    private val binding get() = _binding!!

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        loadMarkers() // DISPLAY MARKERS
        enableLocation()
        if (markerToZoom != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                LatLng(markerToZoom!!.location.latitude, markerToZoom!!.location.longitude),
                18f), 2000, null)
            markerToZoom = null
        }
        map.setOnMapClickListener {
            createPersonalMarker(LatLng(it.latitude, it.longitude), "PersonalMarker")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        createMap()
        MapViewModel.currentMark.currentMarker.value = null // In map, if it there is no marker it'll take the user's location, set it to null will permit to display User location instead of another data like editMarker.

        MapViewModel.currentMark.marks.observe(viewLifecycleOwner) {
            loadMarkers()
        }
        if (currentMarker != null)
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                LatLng(currentMarker!!.position.latitude, currentMarker!!.position.longitude),
                18f), 4000, null)

        return root
    }

    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // CREATE MARK
        binding.buttonCreate.setOnClickListener {
            val locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val criteria = Criteria()
            val location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false)!!)

            if (currentMarker != null)
                MapViewModel.currentMark.currentMarker.postValue(currentMarker)
            else
                if (location != null)
                    MapViewModel.currentMark.currentMarker.postValue(map.addMarker(MarkerOptions()
                        .position(LatLng(location.latitude, location.longitude))
                        .title("Personal Marker")))
                else
                    Toast.makeText(context, "Getting location...", Toast.LENGTH_SHORT).show()
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_homeFragment_to_createMarkerFragment)
        }

        // GO TO MARK LIST
        binding.buttonList.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_homeFragment_to_markListFragment)
        }
    }




    // - - - - - - - - - F U N C T I O N S - - - - - - - - - //

    private fun createPersonalMarker(coordinates: LatLng, title: String) {
        if (currentMarker != null)
            currentMarker?.remove()
        val myMarker = MarkerOptions().position(coordinates).title(title).draggable(true)
        currentMarker = map.addMarker(myMarker)!!
        //viewModel.marks.add()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }

    fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun loadMarkers(){
        MapViewModel.currentMark.marks.value?.forEach {
            val coordinates = LatLng(it.location.latitude,it.location.longitude)
            val myMarker = MarkerOptions().position(coordinates).title(it.markTitle)
            Log.e("HLEOU", it.user.toString())
            if (it.user.equals(MapViewModel.currentUser))
                myMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            Log.i("USERMARK", "${it.user}")
            map.addMarker(myMarker)
            Log.i(it.markTitle, "$it")
            //map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f), 5000, null)
        }
    }



    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }


}