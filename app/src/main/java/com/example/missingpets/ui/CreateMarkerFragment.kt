package com.example.missingpets.ui

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.example.missingpets.R
import com.example.missingpets.databinding.FragmentCreateMarkerBinding
import com.example.missingpets.model.PetMark
import com.example.missingpets.viewmodel.MapViewModel
import com.example.missingpets.viewmodel.MapViewModel.currentMark.currentMarker
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso
import java.io.FileNotFoundException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*


class CreateMarkerFragment : Fragment() {

    private val viewModel: MapViewModel by viewModels()
    private var _binding: FragmentCreateMarkerBinding? = null
    private val db = FirebaseFirestore.getInstance()
    var imageUri: Uri? = null
    var imageName: String = ""

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentCreateMarkerBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(requireActivity(), Array(1){
                Manifest.permission.CAMERA
            }, 100)
        }

        binding.takePhoto.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            startActivityForResult(intent, 100)
        }

        binding.imageTaken.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, 200)
        }

        currentMarker.observe(viewLifecycleOwner) {
            val latitude = currentMarker.value?.position?.latitude
            val longitude = currentMarker.value?.position?.longitude

            binding.latitudeTextview.text = String.format("Latitude: %.6f", latitude)
            binding.longitudeTextview.text = String.format("Longitude: %.6f", longitude)
        }

        binding.saveMark.setOnClickListener {
            if (fieldsAreFilled()) {
                uploadImage()
                val markListSize = MapViewModel.currentMark.marks.value!!.size
                val latitude = currentMarker.value!!.position.latitude
                val longitude = currentMarker.value!!.position.longitude
                val pet = PetMark(null,
                    MapViewModel.currentUser,
                    binding.addDescripiton.text.toString(),
                    binding.addTlf.text.toString(),
                    binding.addEmail.text.toString(),
                    imageName,
                    GeoPoint(latitude, longitude),
                    binding.markTitle.text.toString(),
                    binding.addAddress.text.toString()
                )
                MapViewModel.currentMark.marks.value?.add(pet)
                uploadMarker(pet)
                NavHostFragment.findNavController(this).navigate(R.id.action_createMarkerFragment_to_homeFragment)
            } else
                Toast.makeText(context, "Some field is empty.", Toast.LENGTH_SHORT).show()
        }

        binding.backToMap.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_createMarkerFragment_to_homeFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun fieldsAreFilled(): Boolean {
        return !(binding.markTitle.text.toString().trim() == "" ||
                binding.addAddress.text.toString().trim() == "" ||
                binding.addTlf.text.toString().trim() == "" ||
                binding.addEmail.text.toString().trim() == "" ||
                binding.addDescripiton.text.toString().trim() == "" ||
                binding.imageTaken.drawable == null)
    }

    private fun uploadMarker(marker: PetMark) {
        db.collection("markers").add(marker)
            .addOnSuccessListener {
                    documentReference -> println("Marker created successfully with ID: ${documentReference.id}")
            }
            .addOnFailureListener {
                e -> Log.w("ERROR", "Error creating the marker.", e)
            }
    }

    private fun uploadImage() {
        // Show progress
        val pd: ProgressDialog = ProgressDialog(this.requireActivity())
        pd.setTitle("Uploading image...")
        pd.show()

        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        imageName = formatter.format(now)
        val storage = FirebaseStorage.getInstance().getReference("images/$imageName")
        storage.putFile(imageUri!!)
            .addOnSuccessListener {
                pd.dismiss()
                Snackbar.make(requireActivity().findViewById(android.R.id.content), "Image Uploaded!", Snackbar.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                pd.dismiss()
                Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
            }
            .addOnProgressListener {

                var progressPercent = (100.00 * it.task.snapshot.bytesTransferred.toDouble() / it.task.snapshot.totalByteCount.toDouble())
                pd.setMessage("Percentage: " + progressPercent.toInt() + "%")
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // CHANGE CODE FOR GET DIFFERENT REQUESTS
        if (requestCode == 100) { // Upload From Gallery
            imageUri = data?.data!!
            //binding.imageTaken.setImageURI(imageUri) // handle chosen image
            Picasso.get().load(imageUri).centerCrop().fit().into(binding.imageTaken)
            binding.imageTaken.setPadding(0)
        } else if (requestCode == 200) { // From Camera
            var bitmap: Bitmap? = (data?.extras?.get("data") as Bitmap?)
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context?.contentResolver, bitmap, "ImageTitle", null))
            if (bitmap != null) {
                binding.imageTaken.setImageBitmap(bitmap)
                binding.imageTaken.setPadding(0)
            }
        }
    }
}