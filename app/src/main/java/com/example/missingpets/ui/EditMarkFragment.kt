package com.example.missingpets.ui

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.example.missingpets.R
import com.example.missingpets.databinding.FragmentEditMarkBinding
import com.example.missingpets.model.PetMark
import com.example.missingpets.viewmodel.MapViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class EditMarkFragment : Fragment() {
    private val viewModel: MapViewModel by viewModels()
    private var _binding: FragmentEditMarkBinding? = null
    private var imageUri: Uri? = null
    private val binding get() = _binding!!
    private var imageName: String = ""
    private val db = FirebaseFirestore.getInstance()
    private var imageHaveChanged: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentEditMarkBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mark = MapViewModel.currentMark.editMark.value

        binding.editMarkName.setText(mark?.markTitle)
        binding.editAddress.setText(mark?.address)
        binding.editEmail.setText(mark?.email)
        binding.editTlf.setText(mark?.tlf)
        binding.editDescription.setText(mark?.description)
        downloadImage(mark?.image)

        binding.editImage.setOnClickListener {
            var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, 200)
        }

        binding.saveEdit.setOnClickListener {
            val marker = PetMark(mark!!.id,
                mark.user,
                binding.editDescription.text.toString(),
                binding.editTlf.text.toString(),
                binding.editEmail.text.toString(),
                mark.image,
                mark.location,
                binding.editMarkName.text.toString(),
                binding.editAddress.text.toString()
            )
            updateMarker(marker)
            //Toast.makeText(context, "Mark edited!", Toast.LENGTH_SHORT).show()
            NavHostFragment.findNavController(this).navigate(R.id.action_editMarkFragment_to_markListFragment)
        }

        binding.cancelEdit.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_editMarkFragment_to_markListFragment)
        }

        binding.uploadFromGalleryBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            startActivityForResult(intent, 100)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun downloadImage(imageName: String?) {
        val storage = FirebaseStorage.getInstance().reference.child("images/$imageName")
        val localFile = File.createTempFile("tempImage", "png")
        storage.getFile(localFile).addOnSuccessListener {
            //val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            //binding.editImage.setImageBitmap(bitmap)
            Picasso.get().load(localFile.toUri()).centerCrop().fit().into(binding.editImage)

        }.addOnFailureListener{
            Toast.makeText(context, "Error downloading image!", Toast.LENGTH_SHORT)
                .show()
        }

    }

    private fun uploadImage() {
        // Show progress
        val pd: ProgressDialog = ProgressDialog(this.requireActivity())
        pd.setTitle("Uploading image...")
        pd.show()

        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        imageName = formatter.format(now)
        val storage = FirebaseStorage.getInstance().getReference("images/$imageName")
        storage.putFile(imageUri!!)
            .addOnSuccessListener {
                pd.dismiss()
                Snackbar.make(requireActivity().findViewById(android.R.id.content), "Image Uploaded!", Snackbar.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                pd.dismiss()
                Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
            }
            .addOnProgressListener {

                var progressPercent = (100.00 * it.task.snapshot.bytesTransferred.toDouble() / it.task.snapshot.totalByteCount.toDouble())
                pd.setMessage("Percentage: " + progressPercent.toInt() + "%")
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // CHANGE CODE FOR GET DIFFERENT REQUESTS
        if (requestCode == 100) { // Upload From Gallery
            imageUri = data?.data!!
            //binding.imageTaken.setImageURI(imageUri) // handle chosen image
            Picasso.get().load(imageUri).centerCrop().fit().into(binding.editImage)
            binding.editImage.setPadding(0)
            imageHaveChanged = true
        } else if (requestCode == 200) { // From Camera
            var bitmap: Bitmap? = (data?.extras?.get("data") as Bitmap?)
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context?.contentResolver, bitmap, "ImageTitle", null))
            if (bitmap != null) {
                binding.editImage.setImageBitmap(bitmap)
                binding.editImage.setPadding(0)
            }
            imageHaveChanged = true
        }
    }

    private fun updateMarker(marker: PetMark) {
        if (imageHaveChanged)
            uploadImage()
        db.collection("markers").document(marker.id!!).update(
            mapOf(
                "address" to marker.address,
                "description" to marker.description,
                "email" to marker.email,
                "image" to imageName,
                "markTitle" to marker.markTitle,
                "tlf" to marker.tlf
            )
        )
            .addOnSuccessListener {
                Snackbar.make(requireActivity().findViewById(android.R.id.content), "Marker updated!", Snackbar.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                Toast.makeText(context, "Something went wrong...", Toast.LENGTH_SHORT).show()
                Log.e("Data update failed", it.message.toString())
            }
        //MapViewModel.currentMark.marks.value!!.set(marker, marker)
    }
}