package com.example.missingpets.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.missingpets.R
import com.example.missingpets.adapter.MarkListAdapter
import com.example.missingpets.databinding.FragmentMarkListBinding
import com.example.missingpets.viewmodel.MapViewModel

class MarkListFragment : Fragment() {
    private val viewModel: MapViewModel by viewModels()
    private var _binding: FragmentMarkListBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentMarkListBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = MarkListAdapter(viewModel)
        binding.markRecyclerview.layoutManager = LinearLayoutManager(this.requireContext())
        binding.markRecyclerview.adapter = adapter

        MapViewModel.currentMark.marks.observe(viewLifecycleOwner) {
            adapter.setMarks(it)
        }

        binding.backToMapButton.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_markListFragment_to_homeFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}