package com.example.missingpets.adapter

import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.net.toUri
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.missingpets.R
import com.example.missingpets.model.PetMark
import com.example.missingpets.viewmodel.MapViewModel
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import java.io.File
import com.example.missingpets.viewmodel.MapViewModel.currentMark
import com.example.missingpets.viewmodel.MapViewModel.currentMark.markerToZoom

class MarkListAdapter(viewModel: MapViewModel) : RecyclerView.Adapter<MarkListAdapter.MarkListViewHolder>() {
    val viewModel = viewModel
    private var markList = mutableListOf<PetMark>()

    fun setMarks(marks : List<PetMark>) {
        markList = marks.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_petmark, parent, false)
        return MarkListViewHolder(view)
    }

    override fun onBindViewHolder(holder: MarkListViewHolder, position: Int) {
        holder.bindData(markList[position])
    }

    override fun getItemCount(): Int {
        return markList.size
    }

    inner class MarkListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var image: ImageView = itemView.findViewById(R.id.pet_image)
        var description: TextView = itemView.findViewById(R.id.description)
        var tlf: TextView = itemView.findViewById(R.id.item_tlf)
        var email: TextView = itemView.findViewById(R.id.item_email)
        val editBtn: ImageButton = itemView.findViewById(R.id.item_edit_button)

        fun bindData(petMark: PetMark) {
            //image.setImageDrawable(petMark.photo)
            downloadImage(petMark.image)
            description.text = petMark.description
            description.movementMethod = ScrollingMovementMethod()
            tlf.text = petMark.tlf
            email.text = petMark.email
            if (petMark.user.equals(MapViewModel.currentUser))
                editBtn.visibility = View.VISIBLE

            itemView.setOnClickListener {
                markerToZoom = petMark
                it.findNavController().navigate(R.id.action_markListFragment_to_homeFragment)
            }
            editBtn.setOnClickListener {
                if (petMark.user.equals(MapViewModel.currentUser)) {
                    currentMark.editMark.value = petMark
                    it.findNavController().navigate(R.id.action_markListFragment_to_editMarkFragment)
                } else
                    Toast.makeText(itemView.context, "You can only edit your own markers!", Toast.LENGTH_SHORT).show()
            }
        }

        fun downloadImage(imageName: String?) {
            val storage = FirebaseStorage.getInstance().reference.child("images/$imageName")
            val localFile = File.createTempFile("tempImage", "png")
            storage.getFile(localFile).addOnSuccessListener {
                //val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                //image.setImageBitmap(bitmap)
                Picasso.get().load(localFile.toUri()).centerCrop().fit().into(image)

            }.addOnFailureListener{
                Toast.makeText(itemView.context, "Error downloading image!", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }
}