package com.example.missingpets.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var email: String,
    var password: String
)