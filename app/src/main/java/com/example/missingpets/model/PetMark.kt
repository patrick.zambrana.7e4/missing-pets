package com.example.missingpets.model

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.GeoPoint

data class PetMark(
    var id: String? = null,
    var user: String? = null,
    var description: String,
    var tlf: String?,
    var email: String?,
    var image: String?,
    var location: GeoPoint,
    var markTitle: String,
    var address: String
)